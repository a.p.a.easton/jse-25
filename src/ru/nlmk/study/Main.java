package ru.nlmk.study;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Person person1 = new Person("Ivan", "Ivanov", LocalDate.of(2020,01,01), "a@mail.ru");
        Person person2 = new Person("Petr", "Petrov", LocalDate.of(2020,01,01), "b@mail.ru");
        Person person3 = new Person("Ivan", "Petrov", LocalDate.of(2020,01,01), "c@mail.ru");
        List<Object> persons = new ArrayList<>();
        persons.add(person1);
        persons.add(person2);
        persons.add(person3);
        CsvHandler csvHandler = new CsvHandler();
        csvHandler.write(persons);
        System.out.println(csvHandler.read());
    }
}
