package ru.nlmk.study;

import java.io.*;
import java.lang.reflect.Field;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class CsvHandler {

    private static final String FILE_NAME = "Person.csv";

    public void write(List<Object> persons){
        checkClass(persons);
        try(BufferedOutputStream dataOutputStream = new BufferedOutputStream(new FileOutputStream(FILE_NAME))) {
            Class<?> clazz = Person.class;
            Field[] fields = clazz.getDeclaredFields();
            for (int i = 0; i < fields.length; i++) {
                fields[i].setAccessible(true);
                if (i < fields.length - 1) {
                    dataOutputStream.write(fields[i].getName().concat(",").getBytes());
                } else {
                    dataOutputStream.write(fields[i].getName().concat("\n").getBytes());
                }
            }
            for (int i = 0; i < persons.size(); i++) {
                for(int j = 0; j < fields.length; j++){
                    fields[j].setAccessible(true);
                    if(j < fields.length - 1){
                        dataOutputStream.write(fields[j].get(persons.get(i)).toString().concat(",").getBytes());
                    } else {
                        dataOutputStream.write(fields[j].get(persons.get(i)).toString().concat("\n").getBytes());
                    }
                }
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public List<Person> read(){
        List<Person> result = new ArrayList<>();
        try(BufferedReader bufferedReader = new BufferedReader((new FileReader(FILE_NAME)))){
            String line = bufferedReader.readLine();
            while((line = bufferedReader.readLine()) != null){
                String[] values = line.split(",");
                String[] date = values[2].split("-");
                int[] dateInt = {Integer.valueOf(date[0]), Integer.valueOf(date[1]), Integer.valueOf(date[2])};
                result.add(new Person(values[0], values[1], LocalDate.of(dateInt[0],dateInt[1],dateInt[2]), values[3]));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    private void checkClass(List<Object> objects){
        Class clazz = objects.get(0).getClass();
        for (Object object : objects) {
            if (clazz != object.getClass()){
                throw new IllegalArgumentException();
            }
        }
    }

}
